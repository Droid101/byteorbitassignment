package nkosi.roger.byteorbitassignment.handler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import nkosi.roger.byteorbitassignment.model.ImageModel;

/**
 * Created by ROGER on 12/20/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper{

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "byteorbit";

    // Contacts table name
    private static final String TABLE_IMAGES = "images";

    // Images Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_LOCATION_NAME = "location_name";
    private static final String KEY_IMAGE  = "image";
    private static final String KEY_DATE_ADDED = "add_added";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME ,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLE =" CREATE TABLE " + TABLE_IMAGES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_LOCATION_NAME + " TEXT,"
                + KEY_IMAGE + " BLOB, " + KEY_DATE_ADDED + " TEXT )";
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public void addNew(ImageModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        values.put(KEY_ID, model.get_id());
        values.put(KEY_DATE_ADDED, model.get_date_addedd());
        values.put(KEY_IMAGE, model.getIage());
        values.put(KEY_LOCATION_NAME, model.get_location_name());

        db.insert(TABLE_IMAGES, null, values);
        db.close(); // Closing database connection
    }

    public ImageModel getItem(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_IMAGES, new String[]{KEY_ID, KEY_LOCATION_NAME,
            KEY_DATE_ADDED, KEY_IMAGE}, KEY_ID + "=?", new String[]{String.valueOf(id)},
                null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        ImageModel imageModel = new ImageModel(Integer.parseInt(cursor.getString(0)),cursor.getString(1),
                cursor.getString(2), cursor.getBlob(3)
                );
        return imageModel;
    }

    public List<ImageModel> getAllImages(){
        List<ImageModel> list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM images ORDER BY id";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                ImageModel imageModel = new ImageModel();
                imageModel.set_id(Integer.parseInt(cursor.getString(0)));
                imageModel.set_location_name(cursor.getString(1));
                imageModel.setImage(cursor.getBlob(2));
                imageModel.set_date_addedd(cursor.getString(3));

                //add the image model object
                list.add(imageModel);
            }while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        return list;
    }
}
