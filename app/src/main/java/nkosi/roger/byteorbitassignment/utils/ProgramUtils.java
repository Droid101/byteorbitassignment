package nkosi.roger.byteorbitassignment.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ROGER on 12/20/2016.
 */

public class ProgramUtils {

    private String _Location, street;

    public String getLocation(double lat, double lng, Context context){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> listAddresses = geocoder.getFromLocation(lat, lng, 1);
            if(null!=listAddresses&&listAddresses.size()>0){
                street = listAddresses.get(0).getAddressLine(0);
                _Location = listAddresses.get(0).getLocality(); // returns null on some locations

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        String loc = street + ", " + _Location;
        return street;
    }

    public String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }
}
