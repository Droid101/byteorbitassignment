package nkosi.roger.byteorbitassignment.utils;

import android.util.Log;

import nkosi.roger.byteorbitassignment.Constants;

/**
 * Created by ROGER on 12/18/2016.
 */
public class aLog {
    public static void w (String TAG, String msg){
        if(Constants.LOGGING) {
            if (TAG != null && msg != null)
                Log.w(TAG, msg);
        }
    }
}
