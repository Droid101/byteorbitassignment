package nkosi.roger.byteorbitassignment.controller;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import nkosi.roger.byteorbitassignment.R;
import nkosi.roger.byteorbitassignment.handler.DatabaseHandler;
import nkosi.roger.byteorbitassignment.model.ImageModel;

/**
 * Created by ROGER on 12/20/2016.
 */
public class ImageAdapter extends ArrayAdapter<ImageModel> {

    Context context;
    int layoutResourceId;

    ArrayList<ImageModel> modelArrayList = new ArrayList<>();

    public ImageAdapter(Context context, int resource, ArrayList<ImageModel> data) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.context = context;
        this.modelArrayList = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ImageHolder holder;

        if (row == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ImageHolder();
            holder.dateTakem = (TextView)row.findViewById(R.id.date_added_view);
            holder.imageTaken = (ImageView)row.findViewById(R.id.photo_taken);
            holder.locationName = (TextView)row.findViewById(R.id.location_view);

            row.setTag(holder);
        }else {
            holder = (ImageHolder)row.getTag();
        }

        final ImageModel model = modelArrayList.get(position);
        holder.locationName.setText(model.get_location_name());
        holder.dateTakem.setText(model.get_date_addedd());

        byte[] outImage= model.getIage();
        Log.e("outImage",outImage.toString());
//        base64CODEC = new Base64CODEC();

//        Bitmap bitmap = base64CODEC.Base64ImageFromURL(imageUri);

        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(imageStream);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }

        holder.imageTaken.setImageBitmap(bitmap);
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("id", model._id+""); // yeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeees it works
                int id  = model._id;
                getSingleItem(id);

            }
        });
        return row;
    }

    void getSingleItem(int id){

        DatabaseHandler handler = new DatabaseHandler(context);

        ImageModel model = handler.getItem(id); //did I just use polymorphism

        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.view_details);
        dialog.setTitle("Detail View");

        // set the custom dialog components
        TextView place_taken = (TextView) dialog.findViewById(R.id.detail_place_taken);
        TextView date_taken = (TextView) dialog.findViewById(R.id.detail_date_taken);
        ImageView image = (ImageView)dialog.findViewById(R.id.image_view_details);

        place_taken.setText(model.get_location_name());
        date_taken.setText(model.get_date_addedd());

        byte[] outImage = model.getIage();

        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(imageStream);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }

        image.setImageBitmap(bitmap);

        dialog.show();

    }

    static class ImageHolder {
        ImageView imageTaken;
        TextView locationName, dateTakem;
    }


}
