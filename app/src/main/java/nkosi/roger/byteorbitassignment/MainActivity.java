package nkosi.roger.byteorbitassignment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;

import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nkosi.roger.byteorbitassignment.controller.ImageAdapter;
import nkosi.roger.byteorbitassignment.handler.DatabaseHandler;
import nkosi.roger.byteorbitassignment.model.ImageModel;
import nkosi.roger.byteorbitassignment.model.ImageResponse;
import nkosi.roger.byteorbitassignment.model.Upload;
import nkosi.roger.byteorbitassignment.service.Service;
import nkosi.roger.byteorbitassignment.utils.ProgramUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private FloatingActionButton fab;
    private ImageView imageView;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri capturedImageUri = null;
    private String imageTitle;
    private String imageUri;

    private DatabaseHandler databaseHandler;

    private Upload upload; // Upload object containging image and meta data

    private GoogleApiClient googleApiClient;
    private Location location;

    LocationManager locationManager;
    private ImageAdapter adapter;

    private Double lat, lng;
    private Bitmap bitmap;

    private ProgramUtils utils;

    private ArrayList<ImageModel> imageModelList = new ArrayList<>();

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        utils = new ProgramUtils();
        databaseHandler = new DatabaseHandler(this);



        this.imageView = (ImageView) findViewById(R.id.image);

        this.fab = (FloatingActionButton) findViewById(R.id.take_photo);
        this.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar cal = Calendar.getInstance();
                File file = new File(Environment.getExternalStorageDirectory(), (cal.getTimeInMillis() + ".jpg"));
                imageTitle = new File(Environment.getExternalStorageDirectory(), (cal.getTimeInMillis() + ".jpg")).toString();
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    file.delete();
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

                capturedImageUri = Uri.fromFile(file);

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });


        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        fetchData();

    }


    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//
//            Bitmap photo = (Bitmap) data.getExtras().get("data");
//            imageView.setImageBitmap(photo);
//            imageView.setVisibility(View.VISIBLE);


            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), capturedImageUri);
                if (bitmap != null) {
//                    imageView.setImageBitmap(bitmap);
                    File file = new File(imageTitle);
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);

                    os.close();

                    createUpload(file);


                    new Service(getApplicationContext()).Execute(upload, new UiCallback());

                    Toast.makeText(MainActivity.this, "" + location.getAltitude() + "" + location.getLongitude(), Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(this, "Image not found", Toast.LENGTH_SHORT).show();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }

    private void createUpload(File image) {
        upload = new Upload();

        upload.image = image;
        upload.title = imageTitle;
        upload.description = null;
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this, "is connected", Toast.LENGTH_SHORT).show();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else {
            Toast.makeText(this, "permissions granted", Toast.LENGTH_SHORT).show();
            try {
                location = locationManager.getLastKnownLocation(bestProvider);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (location != null) {
                lat = location.getLatitude();
                lng = location.getLongitude();
            }else {
                Toast.makeText(MainActivity.this, "Location is null", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class UiCallback implements Callback<ImageResponse> {

        @Override
        public void success(ImageResponse imageResponse, Response response) {
            setImageUri(imageResponse.data.link);
            // convert bitmap to byte
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte imageInByte[] = stream.toByteArray();


            databaseHandler.addNew(new ImageModel(utils.getLocation(lat, lng, MainActivity.this), utils.getCurrentTime(), imageInByte));
            fetchData();
//            Toast.makeText(MainActivity.this, getImageUri() + " test", Toast.LENGTH_LONG).show();
//            Toast.makeText(MainActivity.this, utils.getLocation(lat, lng, getApplicationContext()), Toast.LENGTH_SHORT).show();
//            Toast.makeText(MainActivity.this, utils.getCurrentTime(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void failure(RetrofitError error) {
            //Assume we have no connection, since error is null
            if (error == null) {
                Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onStop() {

        googleApiClient.disconnect();
        super.onStop();

    }

    @Override
    protected void onStart() {

        googleApiClient.connect();
        super.onStart();
    }

    void fetchData(){
        List<ImageModel> list = databaseHandler.getAllImages();
        for (ImageModel model : list){
            String log = model.get_id()  + " " + model.get_date_addedd()+model.getIage()+model.get_location_name();
            Log.e("log", log);
            imageModelList.add(model);
        }
        adapter = new ImageAdapter(MainActivity.this, R.layout.image_data_row, imageModelList);
        ListView dataList = (ListView) findViewById(R.id.list);
        dataList.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
//        fetchData();
        super.onResume();
    }
}
