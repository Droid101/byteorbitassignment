package nkosi.roger.byteorbitassignment.model;

/**
 * Created by ROGER on 12/18/2016.
 */
public class ResponseModel {
    String response;

    public ResponseModel(ResponseBuilder builder){
        this.response = builder.response;
    }

    public static class ResponseBuilder{
        String response;

        public ResponseBuilder setResponse(String response){
            this.response = response;
            return ResponseBuilder.this;
        }

        public ResponseModel buildResponse(){
            return new ResponseModel(ResponseBuilder.this);
        }
    }
}
