package nkosi.roger.byteorbitassignment.model;

/**
 * Created by ROGER on 12/20/2016.
 */
public class ImageModel {
    public int _id;
    public String _location_name, _date_addedd ;
    public byte[] _image;

    public ImageModel() {
    }

    public ImageModel(int _id, String _location_name, String dateAdded, byte[] image) {
        this._id = _id;
        this._location_name = _location_name;
        this._date_addedd = dateAdded;
        this._image = image;
    }

    public ImageModel(String _location_name, String dateAdded, byte[] image) {
        this._location_name = _location_name;
        this._date_addedd = dateAdded;
        this._image = image;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_location_name() {
        return _location_name;
    }

    public void set_location_name(String _location_name) {
        this._location_name = _location_name;
    }

    public String get_date_addedd() {
        return _date_addedd;
    }

    public void set_date_addedd(String _date_addedd) {
        this._date_addedd = _date_addedd;
    }

    public byte[] getIage() {
        return _image;
    }

    public void setImage(byte[] image) {
        this._image = image;
    }
}
